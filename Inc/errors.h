/**
  ******************************************************************************
  * File Name          : errors.h
  ******************************************************************************
  */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __ERRORS_H
#define __ERRORS_H
  /* Includes ------------------------------------------------------------------*/
//#include "main.h"
#include "dataRx.h"
#include "DIALOG.h"
//#include "dataTx.h"


#define ERRORS_MESSAGE_SIZE			RX_SENTENCE_PARAMSIZE + RX_SENTENCE_VALUESIZE + 1
#define ERRORS_FLAG_SIZE			RX_SENTENCE_AFLAGSIZE
#define ERRORS_NULL_CHARACTER		0x5F	/* '_' - character */

#define MESSAGE_TYPE_INFO_CH		0x49	/* 'I' */
#define MESSAGE_TYPE_WARN_CH		0x57	/* 'W' */
#define MESSAGE_TYPE_ERR_CH			0x45	/* 'E' */
#define MESSAGE_TYPE_TERM_CH		0x54	/* 'T' */
#define MESSAGE_TYPE_CHECK_CH		0x43	/* 'C' */

#define ERRORS_LIST_COUNT			2


/* settings state */
typedef enum {
	ERRORS_STATE_INACTIVE	= 0x00,
	ERRORS_STATE_ACTIVE	= 0x01,
} ErrorsState;

/* settings state */
typedef enum {
	MSG_TYPE_INFO	= 0x00,
	MSG_TYPE_WARN	= 0x01,
	MSG_TYPE_ERR	= 0x02,
	MSG_TYPE_TERM	= 0x03,
	MSG_TYPE_CHECK	= 0x04,
} MessageType;

/* class for data related to single error */
typedef struct {
	uint8_t number;	/* number of error (1..15) */
	ErrorsState state;
	uint8_t message[ERRORS_MESSAGE_SIZE + 1]; /* message */
	FlagStatus updateData; /* data was updated flag (SET - we got new data, RESET - new data was processed)*/
} Error_t;

/* satellites data set */
typedef struct {
	uint8_t activeList;
	Error_t Errors[RX_SENTENCE_MAXCOUNT][ERRORS_LIST_COUNT];
} ErrorsList_t;
ErrorsList_t ErrorsList;

/* class for data related to single error */
typedef struct {
	MessageType type;
	uint8_t message[ERRORS_MESSAGE_SIZE + 1]; /* message */
	FlagStatus updateData; /* data was updated flag (SET - we got new data, RESET - new data was processed)*/
} Message_t;
Message_t MessageObj;

void parseRxErrorMessage(RxPackageType packageType, uint8_t * package, uint8_t currentNumber, uint8_t totalCount);
void initErrorsList();


#endif /* __ERRORS_H */
