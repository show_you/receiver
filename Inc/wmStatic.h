/**
  ******************************************************************************
  * @file    wmStatic.h
  * @brief   Header for wmStatic.c module
  ******************************************************************************
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __WMSTATIC_H
#define __WMSTATIC_H

/* Includes ------------------------------------------------------------------*/



/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/

#define DEFAULT_BUTTON_FONT	&GUI_Font24_ASCII
#define BUTTON_COUNT		5

#define TARGET_X0		150
#define TARGET_Y0		136
#define TARGET_R0		120
#define TARGET_COLOR    0x00A6DABB
#define TARGET_INN_CIRCLE_R1	(int)TARGET_R0 * 85 / 100
#define TARGET_INN_CIRCLE_R2	(int)TARGET_R0 * 49 / 100
#define TARGET_INN_CIRCLE_COLOR




WM_HWIN CreateMainWindow(void);
WM_HWIN CreatePositionWindow(void);
WM_HWIN CreateSystemWindow(void);
WM_HWIN CreateReferenceWindow(void);
WM_HWIN CreateErrorsWindow(void);
WM_HWIN CreateObservationWindow(void);

#endif /* __WMSTATIC_H */


/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
