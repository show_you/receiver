/**
  ******************************************************************************
  * File Name          : dataRx.h
  ******************************************************************************
  */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __DATARX_H
#define __DATARX_H
  /* Includes ------------------------------------------------------------------*/

#include "stm32746g_discovery.h"


#define RX_PACK_START_DELIMETER		0x3A	/* : */
#define RX_PACK_ELEMENT_DELIMETER	0x3B	/* ; */

/* Size of Reception buffer */
#define RX_BUFFERSIZE			1
/* count of name bytes of receiving packages */
#define RX_TYPESIZE				3

/* satellite type package size */
#define RX_SENTENCE_SATSIZE		75
/* common type package size */
#define RX_SENTENCE_COMMONSIZE	57
/* maximum count of sentences in one package */
#define RX_SENTENCE_MAXCOUNT	15
/* count of name bytes of receiving packages */
#define RX_PACKAGE_DATASIZE			60
/* size of data for single satellite in sentence */
#define RX_SENTENCE_SINGLESATSIZE	10
/* count of satellites in one sentence */
#define RX_SENTENCE_SATCOUNT		6

/* size of data for parameter in sentence */
#define RX_SENTENCE_PARAMSIZE		28
/* size of data for value in sentence */
#define RX_SENTENCE_VALUESIZE		16
/* size of data for availability flag in sentence */
#define RX_SENTENCE_AFLAGSIZE		1

/*  */
#define RX_PACK_SYMBOL_CR	13
#define RX_PACK_SYMBOL_LF	10

/* receiving byte state */
typedef enum {
	RX_NO_BYTE 			= 0x00,		/*  means that there is no currently receiving package, ready to receive new package */
	RX_PACKAGE_START 	= 0x01,		/*  */
	RX_PACKAGE_NAME 	= 0x02,
	RX_PACKAGE_DATA 	= 0x03,
	RX_PACKAGE_CHSUM 	= 0x04,
	RX_COMPLETE 		= 0x05,			/* receiving of package is finished successfully */
	RX_ERROR 			= 0xFF,			/* the sequence of bytes in package is wrong */
}RxReceivePackageState;

#define RX_PACK_TYPESCOUNT		6
/* receiving package types */
typedef enum {
	RX_PACK_OBS		 	= 0x00,		/*  */
	RX_PACK_POS 		= 0x01,		/*  */
	RX_PACK_SYS 		= 0x02,		/*  */
	RX_PACK_REF 		= 0x03,		/*  */
	RX_PACK_ERR 		= 0x04,		/*  */
	RX_PACK_MSG			= 0x05,
	RX_PACK_NO_TYPE 	= 0x06,		/*  */
	RX_PACK_TYPE_ERROR 	= 0xFF,		/*  */
}RxPackageType;

/* types of sentence status, according to its structure */
typedef enum {
	RX_PACK_STRUCT_EMPTY 	= 0x00,		/* sentence buffer was not filled at all, can't be checked for structure status */
	RX_PACK_STRUCT_VALID 	= 0x01,		/* sentence structure is correct */
	RX_PACK_STRUCT_INVALID 	= 0x02,		/* errors in sentence structure */
}RxPackageStructureStatus;

uint8_t packageTypesText[RX_PACK_TYPESCOUNT][RX_TYPESIZE];
//uint8_t typesText = "000OBSSETSYS";

/* parsed received package */
typedef struct {
	uint8_t packageName[RX_TYPESIZE];						/*  */
	RxPackageType interimPackageType;						/* temp. value until we get full name of package */
	RxPackageType packageType;								/*  */
	uint8_t packageNumberInSet;								/*  */
	uint8_t packagesCountInSet;								/*  */
	uint8_t elementsCountInPackage;							/*  */
	uint8_t packageData[RX_PACKAGE_DATASIZE];				/*  */									/*  */
	uint8_t	checksum;										/*  */
	ITStatus checksumIsCorrect;								/* SET - correct checksum, RESET - incorrect checksum */
	uint8_t rxCounter;										/* counter for purpose of filling packageName, packageDataArgument, packageDataValue byte by byte */
	RxPackageStructureStatus structStatus;	/* status of sentences in package, according to its structure */
} RxPackage_t;
RxPackage_t rxPackage[RX_SENTENCE_MAXCOUNT];


void checkReceivingState();
void startUartRx();
void packageStructClear(RxPackage_t * package);
void computeResultAfterReceive();
void parsePackage();

void computeChannelsFine();
void computeChannelsCoarse();
int64_t computeChannelsDifference();
int32_t computeChannelsDifference4Check();

void checkChannelsReceiveState();

uint8_t userConvertIntToChar(uint8_t integer);

#endif /* __DATARX_H */

