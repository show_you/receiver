/**
  ******************************************************************************
  * File Name          : dataTx.h
  ******************************************************************************
  */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __DATATX_H
#define __DATATX_H
  /* Includes ------------------------------------------------------------------*/

//#include "main.h"
#include "stm32746g_discovery.h"
#include "dataRx.h"
#include "settings.h"
#include "errors.h"

/* transmit package size */
#define TXPACKAGESIZE				RX_SENTENCE_COMMONSIZE + 1
#define TX_SENTENCE_PARAMSIZE		SETTINGS_PARAMETER_SIZE
#define TX_SENTENCE_VALUESIZE		SETTINGS_VALUE_SIZE
#define TX_SENTENCE_AFLAGSIZE		SETTINGS_E_FLAG_SIZE
#define TX_NULL_CHARACTER			0x5F	/* '_' - character */
#define TX_PACK_FLAG_CHARACTER		0x21	/* ! */
#define TX_PACK_START_DELIMETER		0x3A	/* : */
#define TX_PACK_ELEMENT_DELIMETER	0x3B	/* ; */
#define TX_SENTENCE_NAMESIZE		8


/* Size of transmission buffer */
#define TXBUFFERSIZE                      TXPACKAGESIZE



HAL_StatusTypeDef txTransferParameter(RxPackageType rxPackageType, char * bufParameter, char * bufValue, ParameterEdType currentDataType);
HAL_StatusTypeDef txTransferMessage(RxPackageType rxPackageType, char * bufValue, MessageType currentMSGType);

void initUartTx();

#endif /* __DATATX_H */
