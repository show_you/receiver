/**
  ******************************************************************************
  * File Name          : settings.h
  ******************************************************************************
  */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __SETTINGS_H
#define __SETTINGS_H
  /* Includes ------------------------------------------------------------------*/
//#include "main.h"
#include "dataRx.h"
#include "DIALOG.h"


#define SETTINGS_PARAMETER_SIZE		RX_SENTENCE_PARAMSIZE
#define SETTINGS_VALUE_SIZE			RX_SENTENCE_VALUESIZE
#define SETTINGS_E_FLAG_SIZE		RX_SENTENCE_AFLAGSIZE
#define NULL_CHARACTER				0x5F	/* '_' - character */

#define PARAM_NOT_EDIT_CHAR		0x4E	/* N */
#define PARAM_INT_EDIT_CHAR		0x49	/* I */
#define PARAM_FLOAT_EDIT_CHAR	0x46	/* F */
#define PARAM_STRING_EDIT_CHAR	0x53	/* S */
#define PARAM_BOOL_EDIT_CHAR	0x42	/* B */
#define PARAM_IP_EDIT_CHAR		0x57	/* W */
#define PARAM_ANTPOS_EDIT_CHAR	0x50	/* P */
#define PARAM_COORD_EDIT_CHAR	0x43	/* C */
#define PARAM_SHUTD_EDIT_CHAR	0x54	/* T */

#define SETTINGS_SET_COUNT			2
#define SETTINGS_TYPE_COUNT			3



/* settings state */
typedef enum {
	PARAMETER_NOT_EDITABLE		= 0x00,
	PARAMETER_INT_EDITABLE		= 0x01,
	PARAMETER_FLOAT_EDITABLE	= 0x02,
	PARAMETER_STRING_EDITABLE	= 0x03,
	PARAMETER_BOOL_EDITABLE		= 0x04,
	PARAMETER_IP_EDITABLE		= 0x05,
	PARAMETER_ANTPOS_EDITABLE	= 0x06,
	PARAMETER_COORD_EDITABLE	= 0x07,
	PARAMETER_SHUTD_EDITABLE	= 0x08,
}ParameterEdType;

/* settings state */
typedef enum {
	SETTINGS_STATE_INACTIVE	= 0x00,
	SETTINGS_STATE_ACTIVE	= 0x01,
}SettingsState;

/* class for data related to single settings */
typedef struct {
	uint8_t number;	/* number of settings (1..15) */
	SettingsState state;
	uint8_t parameter[SETTINGS_PARAMETER_SIZE + 1]; /* parameter */
	uint8_t value[SETTINGS_VALUE_SIZE + 1]; /* value */
	ParameterEdType editType;
	FlagStatus updateData; /* data was updated flag (SET - we got new data, RESET - new data was processed)*/
} Settings_t;

/* satellites data set */
typedef struct {
	RxPackageType type;
	uint8_t activeSet;
	Settings_t Settings[RX_SENTENCE_MAXCOUNT][SETTINGS_SET_COUNT];
}SettingsSet_t;
SettingsSet_t SettingsSet[SETTINGS_TYPE_COUNT];



void initSettings();
void clearSettingsData(RxPackageType packageType);
void parseRxSettingsData(RxPackageType packageType, uint8_t * package, uint8_t currentNumber, uint8_t totalCount);
ParameterEdType convertCharToParameterType(uint8_t character);
uint8_t convertParameterTypeToChar(ParameterEdType type);
void drawListviewHeader(WM_HWIN hItem);




#endif /* __SETTINGS_H */
