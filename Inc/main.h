/**
  ******************************************************************************
  * @file    main.h
  * @brief   Header for main.c module
  ******************************************************************************
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stdio.h"
#include "cmsis_os.h"
#include "cpu_utils.h"
#include "stm32f7xx_hal.h"
#include "stm32f7xx_it.h"
#include "stm32746g_discovery.h"
#include "stm32746g_discovery_lcd.h"
#include "stm32746g_discovery_ts.h"
#include "stm32746g_discovery_sdram.h"
#include "stm32746g_discovery_qspi.h"


/* GUI includes components */
#include "GUI.h"
#include "DIALOG.h"
#include "ST_GUI_Addons.h"
#include "WM.h"
#include "stdint.h"
#include "wmStatic.h"

/* UART transfer user component */
#include "dataRx.h"
#include "dataTx.h"

#include "satellite.h"
#include "settings.h"
#include "errors.h"




/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/

 WM_HWIN CreateWindow(void);
 WM_HWIN CreateWindow0(void);
 WM_HWIN CreateWindowConfig(void);
 WM_HWIN CreateWindowMain(void);
// WM_HWIN CreateMainWindow(void);
// WM_HWIN CreateFirstWindow(void);
// WM_HWIN CreateSecondWindow(void);

/* Exported macro ------------------------------------------------------------*/
#define COUNTOF(__BUFFER__)   (sizeof(__BUFFER__) / sizeof(*(__BUFFER__)))
/* Exported functions ------------------------------------------------------- */


#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */


/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
