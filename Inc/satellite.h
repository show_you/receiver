/**
  ******************************************************************************
  * File Name          : satellite.h
  ******************************************************************************
  */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __SATELLITE_H
#define __SATELLITE_H
  /* Includes ------------------------------------------------------------------*/
//#include "main.h"

#include "dataRx.h"
#include "DIALOG.h"


#define SAT_TOTAL_COUNT_GPS			36
#define SAT_TOTAL_COUNT_GLONASS		36

/* message example
 	 OBS:126:G011807938;G020150937;G141853545;G162353041;G222650737;G233162546;4
 	 OBS:224:G211250840;R220151146;R230554248;R24138441;"
*/

#define SAT_TYPE_BYTE_GPS		0x47	/* G */
#define SAT_TYPE_BYTE_GLONASS	0x52	/* R */
//
#define SAT_TYPE_BYTES_COUNT			1
#define SAT_NUMBER_BYTES_COUNT			2
#define SAT_AZIMUTH_BYTES_COUNT			3
#define SAT_AZIMUTH_BYTES_COUNT			3

#define SAT_SET_COUNT	2

/* type of satellite */
typedef enum {
	SAT_TYPE_NONE 		= 0x00,
	SAT_TYPE_GPS 		= 0x01,
	SAT_TYPE_GLONASS 	= 0x02,
}SatelliteType;

/* satellite state */
typedef enum {
	SAT_STATE_INACTIVE	= 0x00,
	SAT_STATE_ACTIVE	= 0x01,
}SatelliteState;

/* class for data related to single satellite */
typedef struct {
	SatelliteType type;
	uint8_t number;	/* number of satellite (1..36) */
	SatelliteState state;
	uint16_t azimuth; /* azimuth of satellite (0..360) */
	uint8_t angle; /* angle of satellite (0..90) */
	uint8_t SNR; /* signal-to-noise ratio (19..50) */
	FlagStatus updateData; /* data was updated flag (SET - we got new data, RESET - new data was processed)*/

	int x;
	int y;
	IMAGE_Handle imageH;
	TEXT_Handle textH;
	GUI_CONST_STORAGE GUI_BITMAP * imageP;
} Satellite_t;

/* satellites data set */
typedef struct {
	Satellite_t Satellite[SAT_TOTAL_COUNT_GPS + SAT_TOTAL_COUNT_GLONASS];
}SatelliteSet;
SatelliteSet SatSet[SAT_SET_COUNT];

uint8_t orderedSet[SAT_TOTAL_COUNT_GPS + SAT_TOTAL_COUNT_GLONASS];

uint8_t activeSatelliteSet; /* currently active satellites set*/


void initSatellites();
void parseRxSatelliteData(uint8_t * package, uint8_t currentNumber, uint8_t totalCount);
void clearSatelliteData();


#endif /* __SATELLITE_H */

