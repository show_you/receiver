/**
  ******************************************************************************
  * File Name          : dataRx.c
  * Description        :
  ******************************************************************************
  *
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/

#include "dataRx.h"
#include "cmsis_os.h"
#include "stdlib.h"
#include "string.h"
#include "satellite.h"
#include "settings.h"
#include "errors.h"


extern FlagStatus mainWindowReady;
extern WM_HWIN hStartWin;
extern osSemaphoreId startMainWindowSemaphore;
extern WM_HWIN CreateMainWindow(void);
extern void clearWindow();
//extern WM_HWIN CreateStartwin();

FlagStatus initProcess = SET;

/* Private variables ---------------------------------------------------------*/

/* semaphore of UART receiving process */
extern osSemaphoreId packageReceivedSemaphore;
extern UART_HandleTypeDef huart6;


/* flag for initialization UART RX process, package state and channel objects */
ITStatus ReadyForUartRx = RESET;

/* buffer for receiving bytes */
__ALIGN_BEGIN uint8_t aRxBuffer[RX_BUFFERSIZE] __ALIGN_END;

uint8_t buf[1] = {'1'};
uint8_t bufC = 0;

uint8_t activeRxPackage = 0;

/* state of receiving process (what byte have we received) */
RxReceivePackageState ReceivingState = RX_PACKAGE_START;

void writeRxPackageStart(RxPackage_t * package, uint8_t rxByte);
void writeRxPackageType(RxPackage_t * package, uint8_t rxByte);
void writeRxPackageData(RxPackage_t * package, uint8_t rxByte);
uint8_t writeRxPackageChsum(RxPackage_t * package, uint8_t rxByte);
ITStatus checkRxPackageChsum(RxPackage_t * package);
uint8_t userConvertCharToInt(uint8_t character);
uint8_t userConvertIntToChar(uint8_t integer);


uint8_t packageTypesText[RX_PACK_TYPESCOUNT][RX_TYPESIZE] = {
		"OBS",
		"POS",
		"SYS",
		"REF",
		"ERR",
		"MSG",
};


/* function that controls receiving of bytes, forms received package and gives semaphore to computing procedure */
void checkReceivingState()
{
	/* if sentences delimiter is CR or LF */
	if (aRxBuffer[0] == RX_PACK_SYMBOL_CR || aRxBuffer[0] == RX_PACK_SYMBOL_LF)
	{
		return;
	}

	/* identify type of receiving byte and form package */
	switch (ReceivingState) {
		case RX_NO_BYTE :
			break;
		case RX_PACKAGE_START :
			writeRxPackageStart(&rxPackage[activeRxPackage], aRxBuffer[0]);
			break;
		case RX_PACKAGE_DATA :
			writeRxPackageData(&rxPackage[activeRxPackage], aRxBuffer[0]);
			break;
		case RX_PACKAGE_CHSUM :
			if (!writeRxPackageChsum(&rxPackage[activeRxPackage], aRxBuffer[0]))
			{

				if (initProcess == SET)
				{
					initProcess = RESET;
					WM_DeleteWindow(hStartWin);
					mainWindowReady = SET;
//					if(osSemaphoreRelease(startMainWindowSemaphore) == osOK) {}
				}

				/* give semaphore to computing procedure */
				if (rxPackage[activeRxPackage].packageNumberInSet == rxPackage[activeRxPackage].packagesCountInSet || (activeRxPackage + 1) == RX_SENTENCE_MAXCOUNT)
				{
					activeRxPackage = 0;
					if(osSemaphoreRelease(packageReceivedSemaphore) == osOK) {}
				} else
				{
					activeRxPackage += 1;
				}

				ReceivingState = RX_PACKAGE_START;
			}
			break;
		case RX_ERROR :
			break;
		default :
			break;

	}
}

/* function to compute */
void parsePackage()
{
//	buf[0] = '1';
//	HAL_UART_Transmit(&huart6, (uint8_t*)buf, 1, 0xFFFF);

	for (uint8_t i = 0; i < RX_SENTENCE_MAXCOUNT; i++)
	{
		if (rxPackage[i].structStatus == RX_PACK_STRUCT_VALID)
		{
			if (checkRxPackageChsum(&rxPackage[i]))
			{
				RxPackage_t * package = &rxPackage[i];

				switch (rxPackage[i].packageType)
				{
					case RX_PACK_OBS:
						parseRxSatelliteData(package->packageData, rxPackage[i].packageNumberInSet, rxPackage[i].packagesCountInSet);
						break;
					case RX_PACK_POS:
					case RX_PACK_REF:
					case RX_PACK_SYS:
						parseRxSettingsData(rxPackage[i].packageType, package->packageData, rxPackage[i].packageNumberInSet, rxPackage[i].packagesCountInSet);
						break;
					case RX_PACK_ERR:
					case RX_PACK_MSG:
						parseRxErrorMessage(rxPackage[i].packageType, package->packageData, rxPackage[i].packageNumberInSet, rxPackage[i].packagesCountInSet);
						break;
					default :
						break;
				}
			}

			packageStructClear(&rxPackage[i]);
		}
	}
}

/* initialize package state and channels objects and start UART RX procedure */
void startUartRx()
{
	if (ReadyForUartRx == RESET) {

		HAL_StatusTypeDef uartReceiveStatus = HAL_OK;

		for (uint8_t i = 0; i < RX_SENTENCE_MAXCOUNT; i++)
		{
			packageStructClear(&rxPackage[i]);
		}
		uartReceiveStatus = HAL_UART_Receive_IT(&huart6, (uint8_t *)aRxBuffer, RX_BUFFERSIZE);

		if (uartReceiveStatus == HAL_OK)
		{
			ReadyForUartRx = SET;
		}
	}
}

/* initialization of package state object, that contains state of packages for each channel set */
void packageStructClear(RxPackage_t * package)
{
//	memset(rxPackage.packageName, 0, RXTYPESIZE);
//	memset(rxPackage.packageDataArgument, 0, RXARGUMENTSIZE);
//	memset(rxPackage.packageDataValue, 0, RXVALUESIZE);

	package->interimPackageType = RX_PACK_NO_TYPE;
	package->packageType = RX_PACK_NO_TYPE;

	package->packageNumberInSet = 0;
	package->packagesCountInSet = 0;
	package->elementsCountInPackage = 0;
	package->checksum = 0;
	package->rxCounter = 0;

	package->checksumIsCorrect = RESET;

	package->structStatus = RX_PACK_STRUCT_EMPTY;

}

/* save start bytes into package structure object */
void writeRxPackageStart(RxPackage_t * package, uint8_t rxByte)
{
	if (package->packageType == RX_PACK_NO_TYPE)
	{
		if (package->rxCounter < RX_TYPESIZE)
		{
			writeRxPackageType(package, rxByte);
		}
	} else
	{
		switch(package->rxCounter)
		{
			case 3 :
				if (rxByte != RX_PACK_START_DELIMETER)
				{
					package->structStatus = RX_PACK_STRUCT_INVALID;
				}
				break;
			case 4 :
				package->packageNumberInSet = userConvertCharToInt(rxByte);
				if (package->packageNumberInSet > 15)
				{
					package->structStatus = RX_PACK_STRUCT_INVALID;
				}
				break;
			case 5 :
				package->packagesCountInSet = userConvertCharToInt(rxByte);
				if (package->packagesCountInSet > 15)
				{
					package->structStatus = RX_PACK_STRUCT_INVALID;
				}
				break;
			case 6 :
				package->elementsCountInPackage = rxByte - '0';
				if (package->elementsCountInPackage > 6)
				{
					package->structStatus = RX_PACK_STRUCT_INVALID;
				}
				break;
			case 7 :
				package->rxCounter = 0;
				if (rxByte != RX_PACK_START_DELIMETER)
				{
					package->structStatus = RX_PACK_STRUCT_INVALID;
					break;
				} else {
					ReceivingState = RX_PACKAGE_DATA;
					return;
				}
				break;
			default :
				break;
		}

		if (package->structStatus == RX_PACK_STRUCT_INVALID)
		{
			package->rxCounter = 0;
			package->packageType = RX_PACK_NO_TYPE;
			package->structStatus = RX_PACK_STRUCT_EMPTY;

			activeRxPackage = 0;

			buf[0] = '2';
			if(osSemaphoreRelease(packageReceivedSemaphore) == osOK) {}
			ReceivingState = RX_PACKAGE_START;

		} else
		{
			package->rxCounter += 1;
		}
	}
}

/* save package type into RxPackage_t object */
void writeRxPackageType(RxPackage_t * package, uint8_t rxByte)
{
	switch (package->rxCounter)
	{
		case 1 :
		case 2 :
			if (rxByte == packageTypesText[package->interimPackageType][package->rxCounter])
			{
				package->packageName[package->rxCounter] = rxByte;
				package->rxCounter += 1;

				if (package->rxCounter == 3)
				{
					package->packageType = package->interimPackageType;
					package->interimPackageType = RX_PACK_NO_TYPE;
				}
				break;
			} else {
				package->rxCounter = 0;
				package->interimPackageType = RX_PACK_NO_TYPE;
			}
		case 0 :
			for (uint8_t i = 0; i < RX_PACK_TYPESCOUNT; i++)
			{
				if (rxByte == packageTypesText[i][package->rxCounter])
				{
					package->packageName[package->rxCounter] = rxByte;
					package->interimPackageType = i;
					package->rxCounter += 1;
					break;
				}
			}

			if (package->rxCounter == 0)
			{
				activeRxPackage = 0;
				buf[0] = '3';
				if(osSemaphoreRelease(packageReceivedSemaphore) == osOK) {}
			}
			break;
		default :
			break;
	}
}


/* save package data into RxPackage_t object */
void writeRxPackageData(RxPackage_t * package, uint8_t rxByte)
{
	uint8_t isDelimeterPosition = 0;
	uint8_t isLastDelimeter = 0;
	uint8_t packageIndex = 0;

	if (package->packageType == RX_PACK_OBS)
	{
		isDelimeterPosition = package->rxCounter % RX_SENTENCE_SINGLESATSIZE == package->rxCounter / RX_SENTENCE_SINGLESATSIZE - 1;
		isLastDelimeter = package->rxCounter == (RX_SENTENCE_SINGLESATSIZE + 1) * RX_SENTENCE_SATCOUNT - 1;
		packageIndex = package->rxCounter - package->rxCounter / (RX_SENTENCE_SINGLESATSIZE + 1);
	} else if (package->packageType == RX_PACK_ERR || package->packageType == RX_PACK_MSG)
	{
		isDelimeterPosition = package->rxCounter == RX_SENTENCE_PARAMSIZE + RX_SENTENCE_VALUESIZE + 1 || package->rxCounter == RX_SENTENCE_PARAMSIZE + RX_SENTENCE_VALUESIZE + 3;
		isLastDelimeter = package->rxCounter == RX_SENTENCE_PARAMSIZE + RX_SENTENCE_VALUESIZE + RX_SENTENCE_AFLAGSIZE + 2;
		packageIndex = package->rxCounter - package->rxCounter / (RX_SENTENCE_PARAMSIZE + RX_SENTENCE_VALUESIZE + 2);
	} else
	{
		isDelimeterPosition = package->rxCounter == RX_SENTENCE_PARAMSIZE || package->rxCounter == RX_SENTENCE_PARAMSIZE + RX_SENTENCE_VALUESIZE + 1 || package->rxCounter == RX_SENTENCE_PARAMSIZE + RX_SENTENCE_VALUESIZE + 3;
		isLastDelimeter = package->rxCounter == RX_SENTENCE_PARAMSIZE + RX_SENTENCE_VALUESIZE + RX_SENTENCE_AFLAGSIZE + 2;
		packageIndex = package->rxCounter - package->rxCounter / (RX_SENTENCE_PARAMSIZE + 1) - package->rxCounter / (RX_SENTENCE_PARAMSIZE + RX_SENTENCE_VALUESIZE + 2);
	}

	if (isDelimeterPosition)
	{
		if (rxByte != RX_PACK_ELEMENT_DELIMETER)
		{
			package->structStatus = RX_PACK_STRUCT_INVALID;
		} else
		{
			if (isLastDelimeter)
			{
//				package->structStatus = RX_PACK_STRUCT_VALID;
				package->rxCounter = 0;
				ReceivingState = RX_PACKAGE_CHSUM;
			} else
			{
				package->rxCounter += 1;
			}
		}
	} else
	{
		package->packageData[packageIndex] = rxByte;
		package->rxCounter += 1;
	}


	if (package->structStatus == RX_PACK_STRUCT_INVALID)
	{
		package->rxCounter = 0;
		package->packageType = RX_PACK_NO_TYPE;
		package->structStatus = RX_PACK_STRUCT_EMPTY;
		ReceivingState = RX_PACKAGE_START;

		activeRxPackage = 0;

		buf[0] = '4';
		if(osSemaphoreRelease(packageReceivedSemaphore) == osOK) {}
		ReceivingState = RX_PACKAGE_START;

	}

}

uint8_t writeRxPackageChsum(RxPackage_t * package, uint8_t rxByte)
{
	if (package->rxCounter == 0)
	{
		package->checksum = userConvertCharToInt(rxByte) * 16;
		package->rxCounter += 1;
	} else {
		package->checksum += userConvertCharToInt(rxByte);

		package->structStatus = RX_PACK_STRUCT_VALID;
		package->rxCounter = 0;
	}

	return package->rxCounter;
}


/* save received checksum  of package and compare with corresponding RxPackage_t object */
ITStatus checkRxPackageChsum(RxPackage_t * package)
{
	uint8_t sum = 0;
	uint8_t i = 0;

	for (i = 0; i < RX_TYPESIZE; i++)
	{
		sum += package->packageName[i];
	}

	sum += userConvertIntToChar(package->packageNumberInSet);
	sum += userConvertIntToChar(package->packagesCountInSet);
	sum += package->elementsCountInPackage + '0';

	sum += RX_PACK_START_DELIMETER + RX_PACK_START_DELIMETER;

	if (package->packageType == RX_PACK_OBS)
	{
		for (i = 0; i < RX_PACKAGE_DATASIZE; i++)
		{
			sum += package->packageData[i];
		}

		for (i = 0; i < RX_SENTENCE_SATCOUNT; i++)
		{
			sum += RX_PACK_ELEMENT_DELIMETER;
		}
	} else if (package->packageType == RX_PACK_ERR || package->packageType == RX_PACK_MSG)
	{
		sum += RX_PACK_ELEMENT_DELIMETER + RX_PACK_ELEMENT_DELIMETER;

		for (i = 0; i < (RX_SENTENCE_PARAMSIZE + RX_SENTENCE_VALUESIZE + RX_SENTENCE_AFLAGSIZE + 1); i++)
		{
			sum += package->packageData[i];
		}
	} else
	{
		sum += RX_PACK_ELEMENT_DELIMETER + RX_PACK_ELEMENT_DELIMETER + RX_PACK_ELEMENT_DELIMETER;

		for (i = 0; i < (RX_SENTENCE_PARAMSIZE + RX_SENTENCE_VALUESIZE + RX_SENTENCE_AFLAGSIZE); i++)
		{
			sum += package->packageData[i];
		}
	}

	if (package->checksum == sum)
	{
		package->checksumIsCorrect = SET;
	} else
	{
		package->checksumIsCorrect = RESET;
		buf[0] = '5';
	}

	return package->checksumIsCorrect;
}


uint8_t userConvertCharToInt(uint8_t character)
{
	uint8_t integer = 0;
	integer = character - '0';
	if (integer > 9)
	{
		integer = integer - 7;
		if (integer > 15)
		{
			integer = 255;
		}
	}

	return integer;
}

uint8_t userConvertIntToChar(uint8_t integer)
{
	uint8_t character = 0;
	if (integer > 9)
	{
		character = integer + '0' + 7;
	} else
	{
		character = integer + '0';
	}

	return character;
}

