/**
  ******************************************************************************
  * File Name          : satellite.c
  * Description        :
  ******************************************************************************
  *
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/

#include "satellite.h"
#include "DIALOG.h"


extern FlagStatus mainWindowReady;
extern void drawSatellitesNew();
extern WM_HWIN OBSWhItem;
extern FlagStatus satHaveData;
extern uint8_t timeoutSat;

void checkDrawOrder();

void initSatellites()
{
	for (uint8_t j = 0; j < SAT_SET_COUNT; j++)
	{
		for (uint8_t i = 0; i < SAT_TOTAL_COUNT_GPS + SAT_TOTAL_COUNT_GLONASS; i++)
		{
			SatSet[j].Satellite[i].number = i < SAT_TOTAL_COUNT_GPS ? i + 1 : i - SAT_TOTAL_COUNT_GPS + 1;
			SatSet[j].Satellite[i].SNR = 0;
			SatSet[j].Satellite[i].angle = 0;
			SatSet[j].Satellite[i].azimuth = 0;
			SatSet[j].Satellite[i].state = SAT_STATE_INACTIVE;
			SatSet[j].Satellite[i].type = i < SAT_TOTAL_COUNT_GPS ? SAT_TYPE_GPS : SAT_TYPE_GLONASS;
			SatSet[j].Satellite[i].updateData = RESET;
		}
	}
}


void parseRxSatelliteData(uint8_t * package, uint8_t currentNumber, uint8_t totalCount)
{
	uint8_t typeShift = 0;

	satHaveData = SET;
	timeoutSat = 0;

	for (uint8_t i = 0; i < RX_SENTENCE_SATCOUNT; i++)
	{
		if (package[i * 10] == SAT_TYPE_BYTE_GPS || package[i * 10] == SAT_TYPE_BYTE_GLONASS)
		{
			typeShift = package[i * 10] == SAT_TYPE_BYTE_GPS ? 0 : SAT_TOTAL_COUNT_GPS;
			typeShift += (package[i * 10 + 1] - '0') * 10 + (package[i * 10 + 2] - '0') - 1;

			SatSet[!activeSatelliteSet].Satellite[typeShift].azimuth = ((uint16_t)package[i * 10 + 3] - '0') * 100 + ((uint16_t)package[i * 10 + 4] - '0') * 10 + ((uint16_t)package[i * 10 + 5] - '0');
			if (SatSet[!activeSatelliteSet].Satellite[typeShift].azimuth > 360) continue;
			SatSet[!activeSatelliteSet].Satellite[typeShift].angle = (package[i * 10 + 6] - '0') * 10 + (package[i * 10 + 7] - '0');
			if (SatSet[!activeSatelliteSet].Satellite[typeShift].angle > 90) continue;
			SatSet[!activeSatelliteSet].Satellite[typeShift].SNR = (package[i * 10 + 8] - '0') * 10 + (package[i * 10 + 9] - '0');
			if (SatSet[!activeSatelliteSet].Satellite[typeShift].SNR > 50 || SatSet[!activeSatelliteSet].Satellite[typeShift].SNR < 19 ) continue;

			SatSet[!activeSatelliteSet].Satellite[typeShift].state = SAT_STATE_ACTIVE;
			SatSet[!activeSatelliteSet].Satellite[typeShift].updateData = SET;

		} else
		{
			continue;
		}
	}
	if (currentNumber == totalCount)
	{
		clearSatelliteData();
		activeSatelliteSet = !activeSatelliteSet;
		checkDrawOrder();

		if (mainWindowReady == SET)
		{
			drawSatellitesNew();
		}
	}
}


void clearSatelliteData()
{
	for (uint8_t i = 0; i < SAT_TOTAL_COUNT_GPS + SAT_TOTAL_COUNT_GLONASS; i++)
	{
		SatSet[activeSatelliteSet].Satellite[i].state = SAT_STATE_INACTIVE;
		SatSet[activeSatelliteSet].Satellite[i].updateData = RESET;
	}
}


void checkDrawOrder()
{
	uint8_t f = 0;
	uint8_t min = 0;
	uint8_t temporary = 0;

	for (uint8_t k = 0; k < SAT_TOTAL_COUNT_GPS + SAT_TOTAL_COUNT_GLONASS; k++)
	{
		orderedSet[k] = k;
	}

	for (uint8_t j = 0; j < SAT_TOTAL_COUNT_GPS + SAT_TOTAL_COUNT_GLONASS - 1; j++)
	{
		f = 0;
		min = j;
		for (uint8_t i = j; i < SAT_TOTAL_COUNT_GPS + SAT_TOTAL_COUNT_GLONASS - 1 - j; i++)
		{
			if (SatSet[activeSatelliteSet].Satellite[orderedSet[i]].SNR > SatSet[activeSatelliteSet].Satellite[orderedSet[i+1]].SNR)
			{
				temporary = orderedSet[i];
				orderedSet[i] = orderedSet[i + 1];
				orderedSet[i + 1] = temporary;
				f = 1;
			}
			if (SatSet[activeSatelliteSet].Satellite[orderedSet[i]].SNR < SatSet[activeSatelliteSet].Satellite[orderedSet[min]].SNR)
			{
				min = i;
			}
		}
		if (f == 0)
		{
			break;
		}
		if (min != j)
		{
			temporary = orderedSet[j];
			orderedSet[j] = orderedSet[min];
			orderedSet[min] = temporary;
		}
	}
}




