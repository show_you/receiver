/**
  ******************************************************************************
    while(1) { ; }  * File Name          : main.c
  * Description        : Main program body
  ******************************************************************************
  *
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private variables ---------------------------------------------------------*/

/* Declaration of task handlers */

osThreadId GUITaskHandle;
osThreadId UartTaskHandle;
osThreadId BackStageTaskHandle;

osSemaphoreId packageReceivedSemaphore;

TIM_HandleTypeDef htim7;

LTDC_HandleTypeDef hltdc;

UART_HandleTypeDef huart6;

osSemaphoreId startMainWindowSemaphore;


extern FlagStatus initProcess;

//uint8_t buf[1] = {31};

//extern uint8_t aRxBuffer[RX_BUFFERSIZE];

/* Private function prototypes -----------------------------------------------*/
static void SystemClock_Config(void);
static void MPU_Config(void);
static void BSP_Config(void);
static void MX_TIM7_Init(void);
//static void MX_LTDC_Init(void);
static void MX_GPIO_Init(void);
static void MX_USART6_UART_Init(void);

static void GUIThread(void const * argument);
static void UartTask(void const * argument);
static void BackStageTask(void const * argument);

extern WM_HWIN CreateStartwin();
extern void clearWindow();
extern void moveStartSatellites();
extern void drawErrorMessageNew();
extern WM_HWIN CreateSleepScreenwin();
extern void closeSleepWindow();

void k_TouchUpdate(void);

osTimerId lcd_timer;

char str1[30];

TS_StateTypeDef ts;

static GUI_PID_STATE TS_State;

uint8_t ts_press_cnt = 0;

uint8_t timeoutSat = 0;

int timeoutSleep = 0;
FlagStatus sleepScreen = RESET;
int temporarySleepShift = 0;

FlagStatus mainWindowReady = RESET;

extern FlagStatus satHaveData;
FlagStatus satBeenDrawn = RESET;


#if defined ( __ICCARM__ ) /*!< IAR Compiler */
#pragma location=0x20006024
uint8_t ucHeap[ configTOTAL_HEAP_SIZE ];
#elif defined ( __CC_ARM )
uint8_t ucHeap[ configTOTAL_HEAP_SIZE ] __attribute__((at(0x20006024)));
#elif defined ( __GNUC__ )
uint8_t ucHeap[ configTOTAL_HEAP_SIZE ] __attribute__((section(".RamData2")));
#endif



int main(void)
{

	/* MPU Configuration----------------------------------------------------------*/
	MPU_Config();

	/* Enable I-Cache-------------------------------------------------------------*/
	SCB_EnableICache();

	/* Enable D-Cache-------------------------------------------------------------*/
	SCB_EnableDCache();

	/* MCU Configuration----------------------------------------------------------*/

	/* Reset of all peripherals, Initializes the Flash interface and the Systick. */
	HAL_Init();

	/* Configure the system clock */
	SystemClock_Config();

	MX_GPIO_Init();
//	MX_LTDC_Init();
	MX_TIM7_Init();
	MX_USART6_UART_Init();

	/* initialize and config BSP LCD and set start window */
	BSP_Config();

	HAL_TIM_Base_Start_IT(&htim7);


	initSatellites();
	initSettings();
	initErrorsList();

	osSemaphoreDef(SEM0);

	/* Create the semaphore used by the two threads. */
	startMainWindowSemaphore = osSemaphoreCreate(osSemaphore(SEM0) , 1);

	osSemaphoreDef(SEM);
	packageReceivedSemaphore = osSemaphoreCreate(osSemaphore(SEM) , 1);

	/* Create GUI task */
	osThreadDef(GUI_Thread, GUIThread, osPriorityNormal, 0, 3 * 1024);
	GUITaskHandle = osThreadCreate (osThread(GUI_Thread), NULL);

	/* definition and creation of fakeUartTask */
	osThreadDef(Uart, UartTask, osPriorityHigh, 0, configMINIMAL_STACK_SIZE * 5);
	UartTaskHandle = osThreadCreate(osThread(Uart), NULL);

	/* definition and creation of timeTask */
	osThreadDef(BackStage, BackStageTask, osPriorityNormal, 0, configMINIMAL_STACK_SIZE * 5);
	BackStageTaskHandle = osThreadCreate(osThread(BackStage), NULL);

	/* add queues, ... */

	/* Start scheduler */
	osKernelStart();
  
	/* We should never get here as control is now taken by the scheduler */

	/* Infinite loop */
	while (1)
	{
	}

}


/** System Clock Configuration
*/
void SystemClock_Config(void)
{
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_OscInitTypeDef RCC_OscInitStruct;
  HAL_StatusTypeDef ret = HAL_OK;

  /* Enable HSE Oscillator and activate PLL with HSE as source */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 25;
  RCC_OscInitStruct.PLL.PLLN = 400;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  HAL_RCC_OscConfig(&RCC_OscInitStruct);

  ret = HAL_PWREx_EnableOverDrive();

  if(ret != HAL_OK)
  {
    while(1) { ; }
  }

  /* Select PLL as system clock source and configure the HCLK, PCLK1 and PCLK2
     clocks dividers */
  RCC_ClkInitStruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;
  HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_6);
}

/**
  * @brief  Configure the MPU attributes as Write Through for SRAM1/2.
  * @note   The Base Address is 0x20010000 since this memory interface is the AXI.
  *         The Region Size is 256KB, it is related to SRAM1 and SRAM2  memory size.
  * @param  None
  * @retval None
  */
static void MPU_Config(void)
{
  MPU_Region_InitTypeDef MPU_InitStruct;

  /* Disable the MPU */
  HAL_MPU_Disable();

  /* Configure the MPU attributes as WT for SRAM */
  MPU_InitStruct.Enable = MPU_REGION_ENABLE;
  MPU_InitStruct.BaseAddress = 0x20010000;
  MPU_InitStruct.Size = MPU_REGION_SIZE_256KB;
  MPU_InitStruct.AccessPermission = MPU_REGION_FULL_ACCESS;
  MPU_InitStruct.IsBufferable = MPU_ACCESS_NOT_BUFFERABLE;
  MPU_InitStruct.IsCacheable = MPU_ACCESS_CACHEABLE;
  MPU_InitStruct.IsShareable = MPU_ACCESS_NOT_SHAREABLE;
  MPU_InitStruct.Number = MPU_REGION_NUMBER0;
  MPU_InitStruct.TypeExtField = MPU_TEX_LEVEL0;
  MPU_InitStruct.SubRegionDisable = 0x00;
  MPU_InitStruct.DisableExec = MPU_INSTRUCTION_ACCESS_ENABLE;

  HAL_MPU_ConfigRegion(&MPU_InitStruct);

  /* Enable the MPU */
  HAL_MPU_Enable(MPU_PRIVILEGED_DEFAULT);
}




/**
  * @brief  Initializes the STM327546G-Discovery's LCD  resources.
  * @param  None
  * @retval None
  */
static void BSP_Config(void)
{

//	  BSP_LCD_Init();

	/* Initialize the NOR */
	BSP_QSPI_Init();
	BSP_QSPI_MemoryMappedMode();

	/* Initialize the SDRAM */
	BSP_SDRAM_Init();

	/* Initialize the Touch screen */
	BSP_TS_Init(420, 272);

	/* Enable CRC to Unlock GUI */
	__HAL_RCC_CRC_CLK_ENABLE();

	/* Enable Back up SRAM */
//	__HAL_RCC_BKPSRAM_CLK_ENABLE();

}

/**
  * @brief  set the GUI profile
  * @param  None.
  * @retval None.
  */
void k_SetGuiProfile(void)
{
  BUTTON_SetDefaultSkin(BUTTON_SKIN_FLEX);
  DROPDOWN_SetDefaultSkin(DROPDOWN_SKIN_FLEX);

  FRAMEWIN_SetDefaultTextColor(0, GUI_WHITE);
  FRAMEWIN_SetDefaultTextColor(1, GUI_WHITE);

  FRAMEWIN_SetDefaultBarColor(0, 0x00DCA939);
  FRAMEWIN_SetDefaultBarColor(1, 0x00DCA939);

//  FRAMEWIN_SetDefaultFont(&GUI_FontAvantGarde20B);

  FRAMEWIN_SetDefaultClientColor(GUI_WHITE);
  FRAMEWIN_SetDefaultTitleHeight(25);


  WINDOW_SetDefaultBkColor(GUI_WHITE);

  LISTVIEW_SetDefaultGridColor(GUI_WHITE);
  LISTVIEW_SetDefaultBkColor(LISTVIEW_CI_SEL, 0x00DCA939);
  DROPDOWN_SetDefaultColor(DROPDOWN_CI_SEL, 0x00DCA939);
  LISTVIEW_SetDefaultBkColor(LISTVIEW_CI_SELFOCUS, 0x00DCA939);
  DROPDOWN_SetDefaultColor(DROPDOWN_CI_SELFOCUS, 0x00DCA939);
  SCROLLBAR_SetDefaultWidth(8);
//  SCROLLBAR_SetDefaultSTSkin();

  HEADER_SetDefaultBkColor(0x00DCA939);
  HEADER_SetDefaultTextColor(GUI_WHITE);
  HEADER_SetDefaultFont(GUI_FONT_16_1);
//  HEADER_SetDefaultSTSkin();
  SCROLLBAR_SetDefaultColor(0x00DCA939, SCROLLBAR_CI_THUMB);
  SCROLLBAR_SetDefaultColor(0x00DCA939, SCROLLBAR_CI_SHAFT);
  SCROLLBAR_SetDefaultColor(0x00DCA939, SCROLLBAR_CI_ARROW);
//  ST_CHOOSEFILE_SetDelim('/');
  GUI_SetDefaultFont(GUI_FONT_20_ASCII);

//  ST_CHOOSEFILE_SetButtonSize(40, 20);

  TEXT_SetDefaultTextColor(0x00DCA939);
//  TEXT_SetDefaultFont(&GUI_FontLubalGraph20);
}


/**
  * @brief  Start task
  * @param  argument: pointer that is passed to the thread function as start argument.
  * @retval None
  */
static void GUIThread(void const * argument)
{

  /* Initialize GUI */
  GUI_Init();

  WM_MULTIBUF_Enable(1);

  GUI_SelectLayer(0);

  GUI_SetBkColor(GUI_DARKYELLOW);
  GUI_Clear();

  GUI_SelectLayer(1);

  GUI_SetBkColor(0xFFC0C0C0);
  GUI_Clear();

  TS_State.Layer = 1;

  CreateMainWindow();
  CreateStartwin();

  /* Gui background Task */
  while(1) {
    GUI_Exec(); /* Do the background work ... Update windows etc.) */
    if (satHaveData == RESET && satBeenDrawn == RESET)
    {
    	satBeenDrawn = SET;
//    	sendOBSPaint();
    	drawErrorMessageNew();
    }
//	  osDelay(1000);
  }
}


/* UartTask function */
static void UartTask(void const * argument)
{

	/* initialize UART transfer and receive */
	initUartTx();
	startUartRx();

	for(;;)
	{

		if (packageReceivedSemaphore != NULL)
		{
			/* Try to obtain the semaphore */
			osSemaphoreWait(packageReceivedSemaphore , 0);
			if(osSemaphoreWait(packageReceivedSemaphore ,  osWaitForever) == osOK)
			{
				/* proceed received packages and compute result values */
				parsePackage();
			}
		}
	}
}

static void BackStageTask(void const * argument)
{

//	if (startMainWindowSemaphore != NULL)
//	{
//		/* Try to obtain the semaphore */
//		osSemaphoreWait(startMainWindowSemaphore , 0);
//		if(osSemaphoreWait(startMainWindowSemaphore ,  osWaitForever) == osOK)
//		{
//			/* proceed received packages and compute result values */
//			clearWindow();
//			mainWindowReady = SET;
//		}
//	}

	for(;;)
	{
		if (initProcess == SET)
		{
			moveStartSatellites();
			osDelay(150);

			temporarySleepShift = temporarySleepShift > 5 ? 0 : temporarySleepShift + 1;

		} else
		{
			osDelay(1000);

			temporarySleepShift += 1;

			if (satHaveData == SET)
			{
				if (timeoutSat < 30)
				{
					timeoutSat++;
				} else
				{
					satBeenDrawn = RESET;
					satHaveData = RESET;
					timeoutSat = 0;
				}
			}
		}

		if (sleepScreen == RESET)
		{
			if (timeoutSleep < 600)
			{
				timeoutSleep = temporarySleepShift > 5 ? timeoutSleep + 1 : timeoutSleep;
			} else
			{
				sleepScreen = SET;
				CreateSleepScreenwin();
				timeoutSleep = 0;
				temporarySleepShift = 0;
			}
		}
	}
}


/**
  * @brief  Rx Transfer completed callback
  * @param  UartHandle: UART handle
  * @note   This example shows a simple way to report end of DMA Rx transfer, and
  *         you can add your own implementation.
  * @retval None
  */
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *UartHandle)
{
	/* form packages from received bytes */
	checkReceivingState();

	/* check: receive one byte, transmit ("one byte" + 1) */
//	uint8_t buf[RXBUFFERSIZE];
//	buf[0] = aRxBuffer[0];
//	HAL_UART_Transmit(&huart6, (uint8_t*)buf, RXBUFFERSIZE, 0xFFFF);
}

void HAL_UART_ErrorCallback(UART_HandleTypeDef *huart)
{

}

/* TIM7 init function */
static void MX_TIM7_Init(void)
{

  TIM_MasterConfigTypeDef sMasterConfig;

  htim7.Instance = TIM7;
  htim7.Init.Prescaler = 10000;
  htim7.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim7.Init.Period = 200;
  htim7.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim7) != HAL_OK)
  {
	  while(1) { ; }
  }

  sMasterConfig.MasterOutputTrigger = TIM_TRGO_ENABLE;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim7, &sMasterConfig) != HAL_OK)
  {
	  while(1) { ; }
  }

}

/* USART6 init function */
static void MX_USART6_UART_Init(void)
{

	huart6.Instance = USART6;
	huart6.Init.BaudRate = 115200;
	huart6.Init.WordLength = UART_WORDLENGTH_8B;
	huart6.Init.StopBits = UART_STOPBITS_1;
	huart6.Init.Parity = UART_PARITY_NONE;
	huart6.Init.Mode = UART_MODE_TX_RX;
	huart6.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	huart6.Init.OverSampling = UART_OVERSAMPLING_16;
	huart6.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
	huart6.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
	if (HAL_UART_Init(&huart6) != HAL_OK)
	{
		while(1) { ; }
	}

}

/**
  * @brief  Read the coordinate of the point touched and assign their
  *         value to the variables u32_TSXCoordinate and u32_TSYCoordinate
  * @param  None
  * @retval None
  */
void k_TouchUpdate(void)
{
  static GUI_PID_STATE TS_State = {0, 0, 0, 0};
  __IO TS_StateTypeDef  ts;
  uint16_t xDiff, yDiff;

  BSP_TS_GetState((TS_StateTypeDef *)&ts);

  if((ts.touchX[0] >= LCD_GetXSize()) ||(ts.touchY[0] >= LCD_GetYSize()) )
  {
    ts.touchX[0] = 0;
    ts.touchY[0] = 0;
    ts.touchDetected =0;
  }

  xDiff = (TS_State.x > ts.touchX[0]) ? (TS_State.x - ts.touchX[0]) : (ts.touchX[0] - TS_State.x);
  yDiff = (TS_State.y > ts.touchY[0]) ? (TS_State.y - ts.touchY[0]) : (ts.touchY[0] - TS_State.y);


  if((TS_State.Pressed != ts.touchDetected ) ||
     (xDiff > 30 )||
      (yDiff > 30))
  {
    TS_State.Pressed = ts.touchDetected;
    TS_State.Layer = /*SelLayer*/1;
    if(ts.touchDetected)
    {
      TS_State.x = ts.touchX[0];
      TS_State.y = ts.touchY[0];
      GUI_TOUCH_StoreStateEx(&TS_State);
    }
    else
    {
      GUI_TOUCH_StoreStateEx(&TS_State);
      TS_State.x = 0;
      TS_State.y = 0;
    }
  }
}

//
///* LTDC init function */
//static void MX_LTDC_Init(void)
//{
//
//  LTDC_LayerCfgTypeDef pLayerCfg;
//  LTDC_LayerCfgTypeDef pLayerCfg1;
//
//  hltdc.Instance = LTDC;
//  hltdc.Init.HSPolarity = LTDC_HSPOLARITY_AL;
//  hltdc.Init.VSPolarity = LTDC_VSPOLARITY_AL;
//  hltdc.Init.DEPolarity = LTDC_DEPOLARITY_AL;
//  hltdc.Init.PCPolarity = LTDC_PCPOLARITY_IPC;
//  hltdc.Init.HorizontalSync = 40;
//  hltdc.Init.VerticalSync = 9;
//  hltdc.Init.AccumulatedHBP = 53;
//  hltdc.Init.AccumulatedVBP = 11;
//  hltdc.Init.AccumulatedActiveW = 533;
//  hltdc.Init.AccumulatedActiveH = 283;
//  hltdc.Init.TotalWidth = 565;
//  hltdc.Init.TotalHeigh = 285;
//  hltdc.Init.Backcolor.Blue = 0;
//  hltdc.Init.Backcolor.Green = 0;
//  hltdc.Init.Backcolor.Red = 0;
//  if (HAL_LTDC_Init(&hltdc) != HAL_OK)
//  {
//	  while(1) { ; }
//  }
//
//  pLayerCfg.WindowX0 = 0;
//  pLayerCfg.WindowX1 = 480;
//  pLayerCfg.WindowY0 = 0;
//  pLayerCfg.WindowY1 = 272;
//  pLayerCfg.PixelFormat = LTDC_PIXEL_FORMAT_ARGB8888;
//  pLayerCfg.Alpha = 255;
//  pLayerCfg.Alpha0 = 0;
//  pLayerCfg.BlendingFactor1 = LTDC_BLENDING_FACTOR1_PAxCA;
//  pLayerCfg.BlendingFactor2 = LTDC_BLENDING_FACTOR2_PAxCA;
//  pLayerCfg.FBStartAdress = 0;
//  pLayerCfg.ImageWidth = 480;
//  pLayerCfg.ImageHeight = 272;
//  pLayerCfg.Backcolor.Blue = 0;
//  pLayerCfg.Backcolor.Green = 0;
//  pLayerCfg.Backcolor.Red = 0;
//  if (HAL_LTDC_ConfigLayer(&hltdc, &pLayerCfg, 0) != HAL_OK)
//  {
//	  while(1) { ; }
//  }
//
//  pLayerCfg1.WindowX0 = 0;
//  pLayerCfg1.WindowX1 = 272;
//  pLayerCfg1.WindowY0 = 0;
//  pLayerCfg1.WindowY1 = 272;
//  pLayerCfg1.PixelFormat = LTDC_PIXEL_FORMAT_ARGB1555;
//  pLayerCfg1.Alpha = 0;
//  pLayerCfg1.Alpha0 = 0;
//  pLayerCfg1.BlendingFactor1 = LTDC_BLENDING_FACTOR1_PAxCA;
//  pLayerCfg1.BlendingFactor2 = LTDC_BLENDING_FACTOR2_PAxCA;
//  pLayerCfg1.FBStartAdress = 0;
//  pLayerCfg1.ImageWidth = 480;
//  pLayerCfg1.ImageHeight = 272;
//  pLayerCfg1.Backcolor.Blue = 0;
//  pLayerCfg1.Backcolor.Green = 0;
//  pLayerCfg1.Backcolor.Red = 0;
//  if (HAL_LTDC_ConfigLayer(&hltdc, &pLayerCfg1, 1) != HAL_OK)
//  {
//	  while(1) { ; }
//  }
//
//}

static void MX_GPIO_Init(void)
{

  GPIO_InitTypeDef GPIO_InitStruct;

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOE_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOG_CLK_ENABLE();
  __HAL_RCC_GPIOJ_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOK_CLK_ENABLE();
  __HAL_RCC_GPIOF_CLK_ENABLE();
  __HAL_RCC_GPIOI_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOK, GPIO_PIN_3, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOI, GPIO_PIN_1, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOI, GPIO_PIN_12, GPIO_PIN_SET);

  /*Configure GPIO pin : PK3 */
  GPIO_InitStruct.Pin = GPIO_PIN_3;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
  HAL_GPIO_Init(GPIOK, &GPIO_InitStruct);

  /*Configure GPIO pin : PI1 */
  GPIO_InitStruct.Pin = GPIO_PIN_1;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOI, &GPIO_InitStruct);

  /*Configure GPIO pin : PI12 */
  GPIO_InitStruct.Pin = GPIO_PIN_12;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
  HAL_GPIO_Init(GPIOI, &GPIO_InitStruct);

  /*Configure GPIO pin : PI11 */
  GPIO_InitStruct.Pin = GPIO_PIN_11;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOI, &GPIO_InitStruct);

}



/**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM6 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
/* USER CODE BEGIN Callback 0 */

/* USER CODE END Callback 0 */
  if (htim->Instance == TIM6) {
    HAL_IncTick();
  } else if (htim->Instance == TIM7)
  {
	  BSP_TS_GetState(&ts);
	  if (ts.touchDetected)
	  {
	    TS_State.Pressed = ts.touchDetected;
	    TS_State.y = ts.touchY[0];
	    TS_State.x = ts.touchX[0];
	    GUI_PID_StoreState(&TS_State);
	    ts_press_cnt++;
	  }
	  else
	  {
	    if(ts_press_cnt)
	    {
	      TS_State.Pressed = 0;
	      GUI_PID_StoreState(&TS_State);
	      ts_press_cnt=0;
	    }
	  }
  }
/* USER CODE BEGIN Callback 1 */

/* USER CODE END Callback 1 */
}



#ifdef USE_FULL_ASSERT

/**
   * @brief Reports the name of the source file and the source line number
   * where the assert_param error has occurred.
   * @param file: pointer to the source file name
   * @param line: assert_param error line source number
   * @retval None
   */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

}

#endif

/**
  * @}
  */ 

/**
  * @}
*/ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
