/**
  ******************************************************************************
  * File Name          : settings.c
  * Description        :
  ******************************************************************************
  *
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/

#include "settings.h"
#include "DIALOG.h"


extern FlagStatus mainWindowReady;
extern void updatePosList();
extern void updateRefList();
extern void updateSysList();


void initSettings()
{
	for (uint8_t i = 0; i < SETTINGS_TYPE_COUNT; i++)
	{
		SettingsSet[i].type = i + 1;
		SettingsSet[i].activeSet = 0;
		for (uint8_t j = 0; j < RX_SENTENCE_MAXCOUNT; j++)
		{
			SettingsSet[i].Settings[j][SettingsSet[i].activeSet].editType = PARAMETER_NOT_EDITABLE;
			SettingsSet[i].Settings[j][!SettingsSet[i].activeSet].editType = PARAMETER_NOT_EDITABLE;

			SettingsSet[i].Settings[j][SettingsSet[i].activeSet].number = j;
			SettingsSet[i].Settings[j][!SettingsSet[i].activeSet].number = j;

			SettingsSet[i].Settings[j][SettingsSet[i].activeSet].state = SETTINGS_STATE_INACTIVE;
			SettingsSet[i].Settings[j][!SettingsSet[i].activeSet].state = SETTINGS_STATE_INACTIVE;

			SettingsSet[i].Settings[j][SettingsSet[i].activeSet].updateData = RESET;
			SettingsSet[i].Settings[j][!SettingsSet[i].activeSet].updateData = RESET;

			SettingsSet[i].Settings[j][SettingsSet[i].activeSet].parameter[SETTINGS_PARAMETER_SIZE] = 0;
			SettingsSet[i].Settings[j][!SettingsSet[i].activeSet].value[SETTINGS_VALUE_SIZE] = 0;
		}
	}
}


void clearSettingsData(RxPackageType packageType)
{
	for (uint8_t i = 0; i < RX_SENTENCE_MAXCOUNT; i++)
	{
		SettingsSet[packageType - 1].Settings[i][SettingsSet[packageType - 1].activeSet].state = SETTINGS_STATE_INACTIVE;
		SettingsSet[packageType - 1].Settings[i][SettingsSet[packageType - 1].activeSet].updateData = RESET;
	}
}


void parseRxSettingsData(RxPackageType packageType, uint8_t * package, uint8_t currentNumber, uint8_t totalCount)
{
	uint8_t character = 0;

	for (uint8_t i = 0; i < RX_SENTENCE_PARAMSIZE + RX_SENTENCE_VALUESIZE + RX_SENTENCE_AFLAGSIZE; i++)
	{
		character = package[i] == NULL_CHARACTER ? 0 : package[i];

		if (i < RX_SENTENCE_PARAMSIZE)
		{
			SettingsSet[packageType - 1].Settings[currentNumber - 1][!SettingsSet[packageType - 1].activeSet].parameter[i] = character;
		} else if (i >= RX_SENTENCE_PARAMSIZE && i < RX_SENTENCE_PARAMSIZE + RX_SENTENCE_VALUESIZE)
		{
			SettingsSet[packageType - 1].Settings[currentNumber - 1][!SettingsSet[packageType - 1].activeSet].value[i - RX_SENTENCE_PARAMSIZE] = character;
		} else
		{
			SettingsSet[packageType - 1].Settings[currentNumber - 1][!SettingsSet[packageType - 1].activeSet].editType = convertCharToParameterType(character);
		}
	}

	SettingsSet[packageType - 1].Settings[currentNumber - 1][!SettingsSet[packageType - 1].activeSet].state = SETTINGS_STATE_ACTIVE;

	if (currentNumber == totalCount)
	{
		clearSettingsData(packageType);
		SettingsSet[packageType - 1].activeSet = !SettingsSet[packageType - 1].activeSet;

		if (mainWindowReady == SET)
		{
			switch(packageType)
			{
				case RX_PACK_POS :
					updatePosList();
					break;
				case RX_PACK_REF :
					updateRefList();
					break;
				case RX_PACK_SYS :
					updateSysList();
					break;
				default :
					break;
			}
		}
	}
}


ParameterEdType convertCharToParameterType(uint8_t character)
{
	ParameterEdType type = PARAMETER_NOT_EDITABLE;
	switch(character)
	{
		case PARAM_INT_EDIT_CHAR:
			type = PARAMETER_INT_EDITABLE;
			break;
		case PARAM_FLOAT_EDIT_CHAR:
			type = PARAMETER_FLOAT_EDITABLE;
			break;
		case PARAM_STRING_EDIT_CHAR:
			type = PARAMETER_STRING_EDITABLE;
			break;
		case PARAM_BOOL_EDIT_CHAR:
			type = PARAMETER_BOOL_EDITABLE;
			break;
		case PARAM_IP_EDIT_CHAR:
			type = PARAMETER_IP_EDITABLE;
			break;
		case PARAM_ANTPOS_EDIT_CHAR:
			type = PARAMETER_ANTPOS_EDITABLE;
			break;
		case PARAM_COORD_EDIT_CHAR:
			type = PARAMETER_COORD_EDITABLE;
			break;
		case PARAM_SHUTD_EDIT_CHAR:
			type = PARAMETER_SHUTD_EDITABLE;
			break;
		default:
			type = PARAMETER_NOT_EDITABLE;
			break;
	}
	return type;
}


uint8_t convertParameterTypeToChar(ParameterEdType type)
{
	uint8_t character = PARAM_NOT_EDIT_CHAR;
	switch(type)
	{
		case PARAMETER_INT_EDITABLE:
			character = PARAM_INT_EDIT_CHAR;
			break;
		case PARAMETER_FLOAT_EDITABLE:
			character = PARAM_FLOAT_EDIT_CHAR;
			break;
		case PARAMETER_STRING_EDITABLE:
			character = PARAM_STRING_EDIT_CHAR;
			break;
		case PARAMETER_BOOL_EDITABLE:
			character = PARAM_BOOL_EDIT_CHAR;
			break;
		case PARAMETER_IP_EDITABLE:
			character = PARAM_IP_EDIT_CHAR;
			break;
		case PARAMETER_ANTPOS_EDITABLE:
			character = PARAM_ANTPOS_EDIT_CHAR;
			break;
		case PARAMETER_COORD_EDITABLE:
			character = PARAM_COORD_EDIT_CHAR;
			break;
		case PARAMETER_SHUTD_EDITABLE:
			character = PARAM_SHUTD_EDIT_CHAR;
			break;
		default:
			character = PARAM_NOT_EDIT_CHAR;
			break;
	}
	return character;
}


void drawListviewHeader(WM_HWIN hItem)
{
	LISTVIEW_AddRow(hItem, NULL);
	LISTVIEW_DisableRow(hItem, 0);
	LISTVIEW_SetBkColor(hItem, LISTVIEW_CI_DISABLED, 0x00A0A0A0);
	LISTVIEW_SetItemText(hItem, 0, 0, (const char *) "Parameter");
	LISTVIEW_SetItemText(hItem, 1, 0, (const char *) "Value");
	LISTVIEW_SetItemTextColor(hItem, 0, 0, LISTVIEW_CI_DISABLED, 0x00000000);
	LISTVIEW_SetItemTextColor(hItem, 1, 0, LISTVIEW_CI_DISABLED, 0x00000000);

}

