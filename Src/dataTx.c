/**
  ******************************************************************************
  * File Name          : dataTx.c
  * Description        :
  ******************************************************************************
  *
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/

#include "dataTx.h"

/* Private variables ---------------------------------------------------------*/

ITStatus ReadyForUartTx = RESET;

extern UART_HandleTypeDef huart6;
extern uint8_t packageTypesText[RX_PACK_TYPESCOUNT][RX_TYPESIZE];

/* transmit buffer */
__ALIGN_BEGIN uint8_t aTxBuffer[TXBUFFERSIZE] __ALIGN_END;


void writePackageStart(RxPackageType rxPackageType);
void writeCharsWithNullFill(char * buf, int start, int count);
void writePackageEFlag(int currentDataType);
void writePackageChSum();



HAL_StatusTypeDef txTransferParameter(RxPackageType rxPackageType, char * bufParameter, char * bufValue, ParameterEdType currentDataType)
{
	writePackageStart(rxPackageType);
	writeCharsWithNullFill(bufParameter, TX_SENTENCE_NAMESIZE, TX_SENTENCE_PARAMSIZE);
	writeCharsWithNullFill(bufValue, TX_SENTENCE_NAMESIZE + TX_SENTENCE_PARAMSIZE + 1, TX_SENTENCE_VALUESIZE);
	writePackageEFlag(currentDataType);
	writePackageChSum();

	HAL_StatusTypeDef status = HAL_UART_Transmit(&huart6, (uint8_t*)aTxBuffer, TXBUFFERSIZE, 0xFFFF);

//	uint8_t buf1[1] = {'1'};
//	HAL_StatusTypeDef status = HAL_UART_Transmit(&huart6, (uint8_t*)buf1, 1, 0xFFFF);

	return status;
}


void writePackageStart(RxPackageType rxPackageType)
{
	for (int i = 0; i < 3; i++)
	{
		aTxBuffer[i] = packageTypesText[rxPackageType][i];
	}

	aTxBuffer[3] = TX_PACK_START_DELIMETER;
	aTxBuffer[4] = '1';
	aTxBuffer[5] = '1';
	aTxBuffer[6] = '0';
	aTxBuffer[7] = TX_PACK_START_DELIMETER;
}

void writeCharsWithNullFill(char * buf, int start, int count)
{
	int isNull = 0;

	for (int i = 0; i < count; i++)
	{
		if (!isNull)
		{
			if (buf[i] == 0)
			{
				isNull = 1;
				aTxBuffer[start + i] = TX_NULL_CHARACTER;
			} else
			{
				aTxBuffer[start + i] = buf[i];
			}
		} else
		{
			aTxBuffer[start + i] = TX_NULL_CHARACTER;
		}
	}

	aTxBuffer[start + count] = TX_PACK_ELEMENT_DELIMETER;
}

void writePackageEFlag(int currentDataType)
{
	aTxBuffer[TX_SENTENCE_NAMESIZE + TX_SENTENCE_PARAMSIZE + TX_SENTENCE_VALUESIZE + 2] = TX_PACK_FLAG_CHARACTER;
	aTxBuffer[TX_SENTENCE_NAMESIZE + TX_SENTENCE_PARAMSIZE + TX_SENTENCE_VALUESIZE + 3] = TX_PACK_ELEMENT_DELIMETER;
}

void writePackageChSum()
{
	uint8_t checksum = 0;

	for (int i = 0; i < TXPACKAGESIZE - 2; i++)
	{
		checksum += aTxBuffer[i];
	}

	aTxBuffer[TXPACKAGESIZE - 2] = userConvertIntToChar(checksum / 16);
	aTxBuffer[TXPACKAGESIZE - 1] = userConvertIntToChar(checksum % 16);
}



/* initialization of UART TX - transmission of all channels trigger levels via UART */
void initUartTx()
{
	if (ReadyForUartTx == RESET) {

		/* flag need to be set only here, to avoid repeated initialization in future */
		ReadyForUartTx = SET;
	}
}

HAL_StatusTypeDef txTransferMessage(RxPackageType rxPackageType, char * bufValue, MessageType currentMSGType)
{
	writePackageStart(rxPackageType);
	writeCharsWithNullFill(bufValue, TX_SENTENCE_NAMESIZE, TX_SENTENCE_VALUESIZE + TX_SENTENCE_PARAMSIZE + 1);
	writePackageEFlag(currentMSGType);
	writePackageChSum();

	HAL_StatusTypeDef status = HAL_UART_Transmit(&huart6, (uint8_t*)aTxBuffer, TXBUFFERSIZE, 0xFFFF);

	return status;
}



