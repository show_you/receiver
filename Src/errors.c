/**
  ******************************************************************************
  * File Name          : errors.c
  * Description        :
  ******************************************************************************
  *
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/

#include "errors.h"
#include "DIALOG.h"
#include "cmsis_os.h"
#include "dataTx.h"

extern FlagStatus messageWindowActive;
extern FlagStatus mainWindowReady;
extern void updateErrorsList();
extern WM_HWIN CreateMessagewin(MessageType msgType, uint8_t * message);

void clearErrorsList();
void terminateAllProcess();


void initErrorsList()
{

	ErrorsList.activeList = 0;

	for (uint8_t j = 0; j < ERRORS_LIST_COUNT; j++)
	{
		for (uint8_t i = 0; i < RX_SENTENCE_MAXCOUNT; i++)
		{
			ErrorsList.Errors[i][j].message[ERRORS_MESSAGE_SIZE] = 0;
			ErrorsList.Errors[i][j].number = i;
			ErrorsList.Errors[i][j].state = ERRORS_STATE_INACTIVE;
			ErrorsList.Errors[i][j].updateData = RESET;
		}
	}
}


void parseRxErrorMessage(RxPackageType packageType, uint8_t * package1, uint8_t currentNumber, uint8_t totalCount)
{
	/* copy data from package1, because package1 can be modified somewhere else */
	uint8_t package[RX_PACKAGE_DATASIZE];

	for (uint8_t i = 0; i < RX_PACKAGE_DATASIZE; i++)
	{
		package[i] = package1[i];
	}

	if (packageType == RX_PACK_ERR)
	{
		for (uint8_t i = 0; i < ERRORS_MESSAGE_SIZE; i++)
		{
			ErrorsList.Errors[currentNumber - 1][!ErrorsList.activeList].message[i] = package[i] == ERRORS_NULL_CHARACTER ? 0 : package[i];
		}

		ErrorsList.Errors[currentNumber - 1][!ErrorsList.activeList].state = ERRORS_STATE_ACTIVE;

		if (currentNumber == totalCount)
		{
			clearErrorsList();
			ErrorsList.activeList = !ErrorsList.activeList;
			if (mainWindowReady == SET)
			{
				updateErrorsList();
			}
		}
	} else if (packageType == RX_PACK_MSG)
	{
		for (uint8_t i = 0; i < ERRORS_MESSAGE_SIZE; i++)
		{
			MessageObj.message[i] = package[i] == ERRORS_NULL_CHARACTER ? 0 : package[i];
		}

		switch(package[ERRORS_MESSAGE_SIZE])
		{
			case MESSAGE_TYPE_INFO_CH :
				MessageObj.type = MSG_TYPE_INFO;
				break;
			case MESSAGE_TYPE_WARN_CH :
				MessageObj.type = MSG_TYPE_WARN;
				break;
			case MESSAGE_TYPE_ERR_CH :
				MessageObj.type = MSG_TYPE_ERR;
				break;
			case MESSAGE_TYPE_TERM_CH :
				MessageObj.type = MSG_TYPE_TERM;
				break;
			case MESSAGE_TYPE_CHECK_CH :
				MessageObj.type = MSG_TYPE_CHECK;
				break;
			default :
				break;
		}

		if (MessageObj.type == MSG_TYPE_CHECK)
		{
			txTransferMessage(RX_PACK_MSG, "It's alive! ALIVE!!!", MSG_TYPE_CHECK);
		} else if (MessageObj.type != MSG_TYPE_TERM)
		{
			if (mainWindowReady == SET && messageWindowActive == RESET)
			{
				CreateMessagewin(MessageObj.type, MessageObj.message);
			}
		} else
		{
			terminateAllProcess();
		}
	}
}


void clearErrorsList()
{
	for (uint8_t i = 0; i < RX_SENTENCE_MAXCOUNT; i++)
	{
		ErrorsList.Errors[i][ErrorsList.activeList].state = ERRORS_STATE_INACTIVE;
		ErrorsList.Errors[i][ErrorsList.activeList].updateData = RESET;
	}
}


void terminateAllProcess()
{
	osDelay(2000);
	NVIC_SystemReset();
}



