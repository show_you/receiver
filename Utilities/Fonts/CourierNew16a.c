/*********************************************************************
*                SEGGER Microcontroller GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
*                           www.segger.com                           *
**********************************************************************
*                                                                    *
* C-file generated by                                                *
*                                                                    *
*        emWin Font Converter (ST) version 5.40                      *
*        Compiled Mar 17 2017, 15:34:36                              *
*                                                                    *
*        (c) 1998 - 2017 Segger Microcontroller GmbH & Co. KG        *
*                                                                    *
**********************************************************************
*                                                                    *
* Source file: CourierNew16a.c                                       *
* Font:        Courier New                                           *
* Height:      16                                                    *
*                                                                    *
**********************************************************************
*                                                                    *
* Initial font height:  16                                           *
* Character disabled:  0xFEDA (65242)                                *
* Edit/Insert/Right     at character 0x21 (33)                       *
* Edit/Insert/Right     at character 0x21 (33)                       *
* Edit/Insert/Right     at character 0x21 (33)                       *
* Edit/Insert/Right     at character 0x21 (33)                       *
* Edit/Insert/Right     at character 0x21 (33)                       *
* Edit/Insert/Right     at character 0x21 (33)                       *
* Edit/Insert/Right     at character 0x21 (33)                       *
* Edit/Insert/Right     at character 0x21 (33)                       *
* Edit/Insert/Right     at character 0x21 (33)                       *
* Edit/Insert/Right     at character 0x21 (33)                       *
* Edit/Insert/Right     at character 0x21 (33)                       *
* Edit/Insert/Right     at character 0x21 (33)                       *
* Edit/Insert/Right     at character 0x21 (33)                       *
* Edit/Insert/Right     at character 0x21 (33)                       *
* Pixel index of character 0x21 (33) at 0/0 set to 0                 *
* Pixel index of character 0x21 (33) at 1/0 set to 0                 *
* Pixel index of character 0x21 (33) at 1/1 set to 0                 *
* Pixel index of character 0x21 (33) at 0/1 set to 0                 *
* Pixel index of character 0x21 (33) at 15/2 set to 1                *
* Pixel index of character 0x21 (33) at 0/2 set to 0                 *
* Pixel index of character 0x21 (33) at 0/3 set to 0                 *
* Pixel index of character 0x21 (33) at 0/4 set to 0                 *
* Pixel index of character 0x21 (33) at 0/5 set to 0                 *
* Pixel index of character 0x21 (33) at 0/7 set to 0                 *
* Pixel index of character 0x21 (33) at 1/3 set to 0                 *
* Pixel index of character 0x21 (33) at 1/2 set to 0                 *
* Pixel index of character 0x21 (33) at 1/4 set to 0                 *
* Pixel index of character 0x21 (33) at 1/5 set to 0                 *
* Pixel index of character 0x21 (33) at 1/6 set to 1                 *
* Pixel index of character 0x21 (33) at 1/7 set to 0                 *
* Pixel index of character 0x21 (33) at 1/6 set to 0                 *
* Pixel index of character 0x21 (33) at 15/2 set to 0                *
* Pixel index of character 0x21 (33) at 13/3 set to 1                *
* Pixel index of character 0x21 (33) at 13/4 set to 1                *
* Pixel index of character 0x21 (33) at 12/4 set to 1                *
* Pixel index of character 0x21 (33) at 14/4 set to 1                *
* Pixel index of character 0x21 (33) at 14/3 set to 1                *
* Pixel index of character 0x21 (33) at 12/3 set to 1                *
* Pixel index of character 0x21 (33) at 11/3 set to 1                *
* Pixel index of character 0x21 (33) at 10/3 set to 1                *
* Pixel index of character 0x21 (33) at 9/3 set to 1                 *
* Pixel index of character 0x21 (33) at 8/3 set to 1                 *
* Pixel index of character 0x21 (33) at 7/3 set to 1                 *
* Pixel index of character 0x21 (33) at 6/3 set to 1                 *
* Pixel index of character 0x21 (33) at 5/3 set to 1                 *
* Pixel index of character 0x21 (33) at 4/3 set to 1                 *
* Pixel index of character 0x21 (33) at 4/4 set to 1                 *
* Pixel index of character 0x21 (33) at 4/5 set to 1                 *
* Pixel index of character 0x21 (33) at 5/5 set to 1                 *
* Pixel index of character 0x21 (33) at 6/5 set to 1                 *
* Pixel index of character 0x21 (33) at 7/5 set to 1                 *
* Pixel index of character 0x21 (33) at 8/5 set to 1                 *
* Pixel index of character 0x21 (33) at 9/5 set to 1                 *
* Pixel index of character 0x21 (33) at 10/5 set to 1                *
* Pixel index of character 0x21 (33) at 11/5 set to 1                *
* Pixel index of character 0x21 (33) at 12/5 set to 1                *
* Pixel index of character 0x21 (33) at 13/5 set to 1                *
* Pixel index of character 0x21 (33) at 14/5 set to 1                *
* Pixel index of character 0x21 (33) at 11/4 set to 1                *
* Pixel index of character 0x21 (33) at 10/4 set to 1                *
* Pixel index of character 0x21 (33) at 9/4 set to 1                 *
* Pixel index of character 0x21 (33) at 8/4 set to 1                 *
* Pixel index of character 0x21 (33) at 7/4 set to 1                 *
* Pixel index of character 0x21 (33) at 6/4 set to 1                 *
* Pixel index of character 0x21 (33) at 5/4 set to 1                 *
* Pixel index of character 0x21 (33) at 3/4 set to 1                 *
* Pixel index of character 0x21 (33) at 2/4 set to 1                 *
* Pixel index of character 0x21 (33) at 1/4 set to 1                 *
* Pixel index of character 0x21 (33) at 0/4 set to 1                 *
* Pixel index of character 0x21 (33) at 1/3 set to 1                 *
* Pixel index of character 0x21 (33) at 2/3 set to 1                 *
* Pixel index of character 0x21 (33) at 3/3 set to 1                 *
* Pixel index of character 0x21 (33) at 3/5 set to 1                 *
* Pixel index of character 0x21 (33) at 2/5 set to 1                 *
* Pixel index of character 0x21 (33) at 1/5 set to 1                 *
* Pixel index of character 0x21 (33) at 2/6 set to 1                 *
* Pixel index of character 0x21 (33) at 3/7 set to 1                 *
* Edit/Increase/Top     of font.                                     *
* Edit/Decrease/Top     of font.                                     *
* Edit/Delete/Top       at character 0x21 (33)                       *
* Edit/Insert/Top       at character 0x21 (33)                       *
* Edit/Insert/Top       at character 0x21 (33)                       *
* Edit/Insert/Bottom    at character 0x21 (33)                       *
* Pixel index of character 0x21 (33) at 2/3 set to 1                 *
* Pixel index of character 0x21 (33) at 3/2 set to 1                 *
* Pixel index of character 0x21 (33) at 4/1 set to 1                 *
* Pixel index of character 0x21 (33) at 4/2 set to 1                 *
* Pixel index of character 0x21 (33) at 4/3 set to 1                 *
* Pixel index of character 0x21 (33) at 3/3 set to 1                 *
* Pixel index of character 0x21 (33) at 3/7 set to 1                 *
* Pixel index of character 0x21 (33) at 4/7 set to 1                 *
* Pixel index of character 0x21 (33) at 4/9 set to 1                 *
* Pixel index of character 0x21 (33) at 4/8 set to 1                 *
* Pixel index of character 0x21 (33) at 3/8 set to 0                 *
* Pixel index of character 0x21 (33) at 5/8 set to 1                 *
* Pixel index of character 0x21 (33) at 4/9 set to 0                 *
* Pixel index of character 0x21 (33) at 6/9 set to 1                 *
* Pixel index of character 0x21 (33) at 6/8 set to 1                 *
* Pixel index of character 0x21 (33) at 6/7 set to 1                 *
* Pixel index of character 0x21 (33) at 5/7 set to 1                 *
* Pixel index of character 0x21 (33) at 1/6 set to 0                 *
* Pixel index of character 0x21 (33) at 2/7 set to 0                 *
* Pixel index of character 0x21 (33) at 3/7 set to 0                 *
* Pixel index of character 0x21 (33) at 4/8 set to 0                 *
* Pixel index of character 0x21 (33) at 5/8 set to 0                 *
* Pixel index of character 0x21 (33) at 6/9 set to 0                 *
* Pixel index of character 0x21 (33) at 1/6 set to 1                 *
* Pixel index of character 0x21 (33) at 3/7 set to 1                 *
* Pixel index of character 0x21 (33) at 5/8 set to 1                 *
* Pixel index of character 0x21 (33) at 5/9 set to 1                 *
* Pixel index of character 0x21 (33) at 5/9 set to 0                 *
* Pixel index of character 0x21 (33) at 6/9 set to 1                 *
* Pixel index of character 0x21 (33) at 6/9 set to 0                 *
* Pixel index of character 0x21 (33) at 7/9 set to 1                 *
* Pixel index of character 0x21 (33) at 7/8 set to 1                 *
* Pixel index of character 0x21 (33) at 7/7 set to 1                 *
* Pixel index of character 0x21 (33) at 7/9 set to 0                 *
* Pixel index of character 0x21 (33) at 7/8 set to 0                 *
* Pixel index of character 0x21 (33) at 7/7 set to 0                 *
* Pixel index of character 0x21 (33) at 6/9 set to 1                 *
* Pixel index of character 0x21 (33) at 6/9 set to 0                 *
* Pixel index of character 0x21 (33) at 6/8 set to 0                 *
* Pixel index of character 0x21 (33) at 6/7 set to 0                 *
* Pixel index of character 0x21 (33) at 5/8 set to 0                 *
* Pixel index of character 0x21 (33) at 5/7 set to 0                 *
* Pixel index of character 0x21 (33) at 4/8 set to 1                 *
* Pixel index of character 0x21 (33) at 4/9 set to 1                 *
* Pixel index of character 0x21 (33) at 3/8 set to 1                 *
* Pixel index of character 0x21 (33) at 2/7 set to 1                 *
*                                                                    *
**********************************************************************
*/

#include "GUI.h"

#ifndef GUI_CONST_STORAGE
  #define GUI_CONST_STORAGE const
#endif

/* The following line needs to be included in any file selecting the
   font.
*/
extern GUI_CONST_STORAGE GUI_FONT GUI_FontCourierNew16a;

/* Start of unicode area <Basic Latin> */
GUI_CONST_STORAGE unsigned char acGUI_FontCourierNew16a_0020[  1] = { /* code 0020, SPACE */
  ________};

GUI_CONST_STORAGE unsigned char acGUI_FontCourierNew16a_0021[ 20] = { /* code 0021, EXCLAMATION MARK */
  ________,________,
  ____X___,________,
  ___XX___,________,
  __XXX___,________,
  _XXXXXXX,XXXXXXX_,
  XXXXXXXX,XXXXXXX_,
  _XXXXXXX,XXXXXXX_,
  __XXX___,________,
  ___XX___,________,
  ____X___,________};


GUI_CONST_STORAGE GUI_CHARINFO_EXT GUI_FontCourierNew16a_CharInfo[3009] = {
   {   1,   1,   0,  12,   7, acGUI_FontCourierNew16a_0020 } /* code 0020, SPACE */
  ,{  16,  10,   2,   3,  21, acGUI_FontCourierNew16a_0021 } /* code 0021, EXCLAMATION MARK */
};



GUI_CONST_STORAGE GUI_FONT_PROP_EXT GUI_FontCourierNew16a_Prop2 = {
   0x00A0 /* first character */
  ,0x0236 /* last character  */
  ,&GUI_FontCourierNew16a_CharInfo[ 95] /* address of first character */
  ,(GUI_CONST_STORAGE GUI_FONT_PROP_EXT *)0 /* pointer to next GUI_FONT_PROP_EXT */
};

GUI_CONST_STORAGE GUI_FONT_PROP_EXT GUI_FontCourierNew16a_Prop1 = {
   0x0020 /* first character */
  ,0x007E /* last character  */
  ,&GUI_FontCourierNew16a_CharInfo[  0] /* address of first character */
  ,&GUI_FontCourierNew16a_Prop2 /* pointer to next GUI_FONT_PROP_EXT */
};

GUI_CONST_STORAGE GUI_FONT GUI_FontCourierNew16a = {
   GUI_FONTTYPE_PROP_EXT /* type of font    */
  ,16 /* height of font  */
  ,16 /* space of font y */
  ,1 /* magnification x */
  ,1 /* magnification y */
  ,{&GUI_FontCourierNew16a_Prop1}
  ,12 /* Baseline */
  ,6 /* Height of lowercase characters */
  ,8 /* Height of capital characters */
};

